const mongoose = require("mongoose");
//mongoose.pluralize(null);

const Schema = mongoose.Schema;

//mongoose.ObjectId.get(v => v.toString());

const FragmentSchema = new Schema({
	title: Object,
	content: Object,
	//order: Number,
	context: Array,
	meta: Object
},{
	minimize: false
});

const NovellaSchema = new Schema({
	title: Object,
	meta: Object,
	fragments: [FragmentSchema]
}, {
	minimize: false
});

const Fragment = mongoose.model("Fragment", FragmentSchema);
const Novella = mongoose.model("Novella", NovellaSchema);

module.exports = {
	Fragment,
	Novella
};
