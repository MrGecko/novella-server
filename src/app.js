const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
const dotenv = require('dotenv');

const {Fragment, Novella} = require("./models");
const mongoose = require('mongoose').set('debug', true);
const ObjectId = require('mongodb').ObjectID;

dotenv.config();
const url = process.env.MONGODB_URI || 'mongodb://localhost:27017/novella';
const port = process.env.PORT || 8090;



mongoose.connect(url, {useNewUrlParser: true});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", function (callback) {
	console.log("Connection Succeeded to", url);
});


const app = express();
app.use(morgan("combined"));
app.use(bodyParser.json());
app.use(cors());

app.get("/novellas", (req, res) => {
	Novella.find({}, function (error, novellas) {
		if (error) {
			return res.send({success: false, error: error})
		}
		return res.send({
			novellas: novellas.map(n => {return {_id: n._id, title: n.title}}),
			success: true
		})
	})
});

app.get("/novellas/:novId", (req, res) => {
	
	Novella.findOne({_id: ObjectId(req.params.novId)}, function (error, novella) {
		if (error || !novella) {
			return res.send({success: false, error: error})
		}
		return res.send({
			novella: novella,
			success: true
		})
	});
	
});

app.put("/novellas/:novId", (req, res) => {
	
	Novella.findOne({_id: ObjectId(req.params.novId)}, function (error, novella) {
		if (error || !novella) {
			return res.send({success: false, error: error})
		}
		novella.title = req.body.title;
		novella.meta = req.body.meta;
		
		novella.save((error) => {
			if (!!error) {
				res.send({success: false, error: error});
			} else {
				res.send({
					novella: novella,
					success: true
				})
			}
		});
		
	});
	
});

// Add new novella
app.post('/novellas', (req, res) => {
	const newNovella = new Novella(req.body);
	newNovella.save((error) => {
		if (!!error) {
			res.send({success: false, error: error});
		} else {
			res.send({
				success: true,
				message: 'Novella saved successfully!',
				novellaId: newNovella._id
			})
		}
	});
});


app.get("/novellas/:novId/fragments/:fragId", (req, res) => {
	Novella.findOne({_id: ObjectId(req.params.novId)}, function (error, novella) {
		if (error || !novella) {
			return res.send({success: false, error: error})
		}
		const fragment = novella.fragments.id(req.params.fragId);
		if (!fragment) {
			return res.send({success: false, error: 'fragment does not exist'})
		}
		return res.send({
			fragment: fragment,
			success: true
		})
	})
});


// Add new fragment
app.post('/novellas/:novId/fragments', (req, res) => {
	Novella.findById({_id: ObjectId(req.params.novId)}, function (error, novella) {
		if (error || !novella) {
			return res.send({success: false, error: error})
		}
		
		const newFragment = new Fragment(req.body);
		
		novella.fragments.push(newFragment);
		
		novella.save((error) => {
			if (error) {
				return res.send({success: false, error: error})
			}
			return res.send({
				success: true,
				message: 'Fragment saved successfully!',
				fragment: newFragment
			})
		});
		
	});
});

// Update a fragment
app.put('/novellas/:novId/fragments/:fragId', (req, res) => {
	Novella.findOne({_id: ObjectId(req.params.novId)}, function (error, novella) {
		if (error || !novella) {
			return res.send({success: false, error: error})
		}
		let fragment = novella.fragments.id(req.params.fragId);
		fragment = Object.assign(fragment, req.body);
		
		novella.save((error) => {
			if (error) {
				return res.send({success: false, error: error})
			}
			return res.send({
				success: true,
				message: 'Fragment updated successfully!',
				fragment: fragment
			})
		});
		
	});
});

// delete a novella
app.delete("/novellas/:novId", (req, res) => {
	Novella.remove({_id: ObjectId(req.params.novId)}, function (error) {
		if (error) {
			res.send({success: false, error: error})
		}
		res.send({success: true})
	})
});

// delete a fragment
app.delete("/novellas/:novId/fragments/:fragId", (req, res) => {
	Novella.findOne({_id: ObjectId(req.params.novId)}, function (error, novella) {
		if (error || !novella) {
			return res.send({success: false, error: error})
		}
		
		let fragment = novella.fragments.id(req.params.fragId);
		if (!fragment) {
			return res.send({success: false, error: 'fragment does not exist'});
		}
		
		fragment.remove(function (error){
			novella.save((error) => {
				if (error) {
					return res.send({success: false})
				}
				return res.send({
					success: true,
					message: 'Fragment removed successfully!',
					fragmentId: fragment._id
				})
			});
			
		});

	});
});



app.listen(port);
